var express = require('express');
var router = express.Router();

/* GET slides listing. */
router.get('/', function(req, res) {
  res.render('slides', { active: true });
});


module.exports = router;
