var express = require('express');
var router = express.Router();

/* GET slides listing. */
router.get('/', function(req, res) {
  res.render('work', { active: true });
});


module.exports = router;
